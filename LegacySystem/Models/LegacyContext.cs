﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace LegacySystem.Models
{
    public class LegacyContext : DbContext
    {
        public LegacyContext(DbContextOptions<LegacyContext> options)
            : base(options)
        {

        }

        public DbSet<Legacy> LegacyItems { get; set; }
    }
}
