﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerOrder.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CustomerOrder.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerOrdersController : ControllerBase
    {
        //Order history
        //create invoices
        //purchase a product
        //check stock as customer
        //refer another user
        private List<CustomerOrderModel> _customerOrders = new List<CustomerOrderModel>();

        public CustomerOrdersController()
        {
            _customerOrders = new List<CustomerOrderModel>() {
                new CustomerOrderModel() { CustomerOrderId = 1, OrderDate = DateTime.Now.AddDays(-3), Amount = 10.99M },
                new CustomerOrderModel() { CustomerOrderId = 2, OrderDate = DateTime.Now.AddDays(-2), Amount = 19.99M },
                new CustomerOrderModel() { CustomerOrderId = 3, OrderDate = DateTime.Now.AddDays(-1), Amount = 4.99M }
            };
        }

        // GET: api/CustomerOrders
        [HttpGet]
        public IEnumerable<CustomerOrderModel> Get()
        {
            return _customerOrders;
        }

        // GET: api/CustomerOrders/5
        [HttpGet("{id}", Name = "Get")]
        public CustomerOrderModel Get(int id)
        {
            return _customerOrders.FirstOrDefault(c => c.CustomerOrderId == id);
        }

        // POST: api/CustomerOrders
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/CustomerOrders/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
